def is_double_pol(number):
    str_num = str(number)
    str_bin_num = str(bin(number))[2:]
    return str_num == str_num[::-1] and str_bin_num == str_bin_num[::-1]


sum = 0
for i in range(1000000):
    if is_double_pol(i):
        sum += i
        print(i, bin(i))
print(sum)
