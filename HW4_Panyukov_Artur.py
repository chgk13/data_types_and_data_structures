def min_number(string):
    '''
    Функиця работает за O(n), где n - количество переданных чисел.
    Множество делается за O(n), функция map заменяет простой цикл
    по списку (для краткости). Проверка наличия элемента в множестве
    происходит за O(1). Худший случай, когда в множестве содержатся
    все числа от 1 до n. Тогда нужно будет выполнить n+1 проверку.
    В итоге получаем O(n).
    '''
    lst_of_numbers = string.split(' ')
    set_of_numbers = set(map(int, lst_of_numbers))

    min = 1
    while True:
        if min not in set_of_numbers:
            print(min)
            break
        else:
            min += 1


while True:
    string = input('-> ')
    if string == 'cancel':
        print('Bye!')
        break
    else:
        min_number(string)
