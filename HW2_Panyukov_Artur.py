def most_frequent_word(string):
    # Formating input string.
    punctuation_marks = '.,!?:'
    for mark in punctuation_marks:
        string = string.replace(mark, '')
    string = string.replace(' - ', ' ')
    string = string.lower()
    string = string.strip()
    while '  ' in string:
        string = string.replace('  ', ' ')

    # Creating dictionary, that contains frequency of the words
    lst_of_words = string.split(' ')
    dict_of_freq = {}
    for word in lst_of_words:
        if word not in dict_of_freq:
            dict_of_freq[word] = 1
        else:
            dict_of_freq[word] += 1

    # Creating most frequent word
    lst_of_most_frq_words = []
    max_num_of_words = 0
    for word in dict_of_freq:
        if dict_of_freq[word] == max_num_of_words:
            lst_of_most_frq_words.append(word)
        elif dict_of_freq[word] > max_num_of_words:
            max_num_of_words = dict_of_freq[word]
            lst_of_most_frq_words = [word]

    final_string = ', '.join(lst_of_most_frq_words)
    print(f'{max_num_of_words} - {final_string}')


while True:
    string = input('-> ')
    if string == 'cancel':
        print('Bye!')
        break
    else:
        most_frequent_word(string)
