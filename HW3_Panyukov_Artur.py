def sum_of_numbers_in_string(string):
    number = []
    sum = None
    for char in string:
        if char.isdigit():
            number.append(char)
        else:
            if number != []:
                if sum is None:
                    sum = int(''.join(number))
                else:
                    sum += int(''.join(number))
                number = []
    if number != []:
        if sum is None:
            sum = int(''.join(number))
        else:
            sum += int(''.join(number))
    if sum is not None:
        print(sum)
    else:
        return


def sum_of_numbers_in_string_v2(string):
    '''
    This function recognizes minuses. Also, it's a different approach.
    '''
    i = 0
    have_digit = False
    sum = 0
    while i < len(string)-1:
        find_digit = False
        if string[i].isdigit():
            start = i
            find_digit = True
            have_digit = True
            while i < len(string) and string[i].isdigit():
                i += 1
        if find_digit:
            number = int(string[start:i])
            if start > 0 and string[start - 1] == '-':
                number *= -1
            sum += number
        i += 1
    if have_digit:
        print(sum)
    else:
        return


while True:
    string = input('-> ')
    if string == 'cancel':
        print('Bye!')
        break
    else:
        sum_of_numbers_in_string_v2(string)
