def unique_words():
    inp = input('Введите текст: ')
    lst = inp.split(' ')
    lst = list(set(lst))
    print(' '.join(lst))


def unique_words_orderd():
    inp = input('Введите текст: ')
    lst = inp.split(' ')
    unique_lst = []
    for word in lst:
        if word not in unique_lst:
            unique_lst.append(word)
    print(' '.join(unique_lst))


unique_words()
unique_words_orderd()
